﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObronLewaka
{
    class Korwin
    {
        private String[] element_1 = new String[22];
        private String[] element_2 = new String[22];
        private String[] element_3 = new String[22];
        private String[] element_4 = new String[22];
        private String[] element_5 = new String[22];
        private String[] element_6 = new String[22];


        public double hit = 0;

        public Korwin()
        {
            element_1[0] = "Proszę zwrócić uwagę, że ";
            element_1[1] = "I tak mam trzy razy mniej czasu, więc proszę mi pozwolić powiedzieć: ";
            element_1[2] = "Państwo się śmieją, ale ";
            element_1[3] = "Ja nie potrzebowałem edukacji seksualnej, żeby wiedzieć, że ";
            element_1[4] = "No niestety: ";
            element_1[5] = "Gdzie leży przyczyna problemy?: Ja państwu powiem: ";
            element_1[6] = "Państwo chyba nie wiedzą, że ";
            element_1[7] = "Oświadczam kategorycznie: ";
            element_1[8] = "Powtarzam: ";
            element_1[9] = "Powiedzmy to całą mocą: ";
            element_1[10] = "W Polsce dzisiaj: ";
            element_1[11] = "Państwo sobie nie zdają sprawy, że ";
            element_1[12] = "To ja przepraszam bardzo: ";
            element_1[13] = "Otóż nie wiem, czy pan wie, że ";
            element_1[14] = "Yyyyy... ";
            element_1[15] = "Ja chcę powiedzieć jedną rzecz: ";
            element_1[16] = "Trzeba powiedzieć jasno: ";
            element_1[17] = "Jak powiedział wybitny krakownianin Stanisław Lem, ";
            element_1[18] = "Proszę mnie dobrze zrozumieć: ";
            element_1[19] = "Ja chciałem państwu przypomnieć, że ";
            element_1[20] = "Niech państwo nie mają złudzeń: ";
            element_1[21] = "Powiedzmy to wyraźnie: ";

            element_2[0] = "właściciele niewolników ";
            element_2[1] = "związkowcy ";
            element_2[2] = "trockiści ";
            element_2[3] = "tak zwane dzieci kwiaty ";
            element_2[4] = "rozmaici urzędnicy ";
            element_2[5] = "federaści ";
            element_2[6] = "etatyści ";
            element_2[7] = "ci durnie i złodzieje ";
            element_2[8] = "ludzie wybrani głosami meneli spod budki z piwem ";
            element_2[9] = "socjaliści pobożni ";
            element_2[10] = "socjaliści bezbożni ";
            element_2[11] = "komuniści z krzyżem w zębach ";
            element_2[12] = "agenci obcych służb ";
            element_2[13] = "członkowie Bandy Czworga ";
            element_2[14] = "pseudo-masoni z Wielkiego Wschodu Francji ";
            element_2[15] = "przedstawiciele czerwonej hołoty ";
            element_2[16] = "ci wszyscy (tfu!) geje ";
            element_2[17] = "funkcjonariusze reżymowej telewizji ";
            element_2[18] = "tak zwani ekolodzy ";
            element_2[19] = "ci wszyscy (tfu!) demokraci ";
            element_2[20] = "agenci bezpieki ";
            element_2[21] = "feminazistki ";

            element_3[0] = "po przeczytaniu Manifestu komunistycznego ";
            element_3[1] = "którymi się brzydze ";
            element_3[2] = "których nienawidzę ";
            element_3[3] = "z okolic \"Gazety Wyborczej\"";
            element_3[4] = "czyli taka żydokomuna ";
            element_3[5] = "odkąd zniesiono karę śmierci ";
            element_3[6] = "którymi pogardzam ";
            element_3[7] = "których miejsce w normalnym kraju jest w więzieniu ";
            element_3[8] = "na polecenie Brukseli ";
            element_3[9] = "posłusznie ";
            element_3[10] = "bezmyślnie ";
            element_3[11] = "z nieprawdopodobną pogardą dla człowieka ";
            element_3[12] = "za pieniądze podatników ";
            element_3[13] = "zgodnie z ideologią LGBTQZ ";
            element_3[14] = "za wszelką cenę ";
            element_3[15] = "zupełnie bezkarnie ";
            element_3[16] = "całkowicie bezczelnie ";
            element_3[17] = "o poglądach na lewo od komunizmu ";
            element_3[18] = "celowo i świadomie ";
            element_3[19] = "z premedytacją ";
            element_3[20] = "od czasów Okrągłego Stołu ";
            element_3[21] = "w ramach postępu ";


            element_4[0] = "udają homoseksualistów ";
            element_4[1] = "niszczą rodzinę ";
            element_4[2] = "idą do polityki ";
            element_4[3] = "zakazują góralom robienia oscypków ";
            element_4[4] = "organizują paraolimpiady ";
            element_4[5] = "wprowadzają ustrój w którym raz na cztery lata można wybrać sobie pana ";
            element_4[6] = "ustawiają fotoradary ";
            element_4[7] = "wprowadzają dotację ";
            element_4[8] = "wydzielają buspasy ";
            element_4[9] = "podnoszą wiek emerytalny ";
            element_4[10] = "rżną głupa ";
            element_4[11] = "odbierają dzieci rodzicom ";
            element_4[12] = "wprowadzają absurdalne przepisy ";
            element_4[13] = "umieszczają dzieci w szkołach koedukacyjnych ";
            element_4[14] = "wprowadzają parytety ";
            element_4[15] = "nawołują do podniesienia podatków ";
            element_4[16] = "próbują wyrzucić kierowców z miast ";
            element_4[17] = "próbują skłócić Polskę z Rosją ";
            element_4[18] = "głoszą brednie o globalnym ociepleniu ";
            element_4[19] = "zakazują posiadania broni ";
            element_4[20] = "nie dopuszczają prawicy do władzy ";
            element_4[21] = "uczą dzieci homoseksualizmu ";

            element_5[0] = "żeby poddawać wszystkich tresurze ";
            element_5[1] = "bo taka jest ich natura ";
            element_5[2] = "bo chcą wszystko kontrolować ";
            element_5[3] = "bo nie rozumieją, że socjalizm nie działa ";
            element_5[4] = "żeby wreszcie zapanował socjalizm ";
            element_5[5] = "dokładnie tak jak towarzysz Janosik ";
            element_5[6] = "zamiast pozwolić ludziom zarabiać ";
            element_5[7] = "żeby wyrwać kobiety z domu ";
            element_5[8] = "bo to jest w interesie tak zwanych ludzi pracy ";
            element_5[9] = "zamiast pozwolić decydować konsumentowi ";
            element_5[10] = "żeby nie opłacało się mieć dzieci ";
            element_5[11] = "zamiast obniżać podatki ";
            element_5[12] = "bo nie rozumieją, że selekcja naturalna jest czymś dobrym ";
            element_5[13] = "żeby mężczyźni przestali być agresywni ";
            element_5[14] = "bo dzięki temu mogą brać łapówki ";
            element_5[15] = "bo dzięki temu mogą kraść ";
            element_5[16] = "bo dostają za to pieniądze ";
            element_5[17] = "bo tak się uczy w państwowej szkole ";
            element_5[18] = "bo bez tego (tfu!) demokracja nie może istnieć ";
            element_5[19] = "bo głupich jest więcej niż mądrych ";
            element_5[20] = "bo chcą tworzyć raj na ziemi ";
            element_5[21] = "bo chcą niszczyć cywilizację białego człowieka ";

            element_6[0] = "co ma zresztą tyle samo sensu, co zawody w szachach dla debili.";
            element_6[1] = "co zostało dokłądnie zaplanowane w Magdaalence przez śp. generała Kiszczaka.";
            element_6[2] = "i trzeba być idiotą, żeby ten system popierać.";
            element_6[3] = "ale nawet ja jeszcze dożyję normalnych czasów.";
            element_6[4] = "co dowodzi, że wyskrobano nie tych co trzeba";
            element_6[5] = "a zwykłym ludziom wmawiają, że im coś \"dadzą\".";
            element_6[6] = "- cóż: chcieliście (tfu!) demokrację, to macie.";
            element_6[7] = "dlatego trzeba zlikwidować koryto, a nie zmieniać świnie.";
            element_6[8] = "a wystarczyłoby przestać wypłacać zasiłki.";
            element_6[9] = "podczas gdy normani ludzie uważani są za dziwaków.";
            element_6[10] = "co w wieku XIX po porstu by wyśmiano.";
            element_6[11] = "- dlatego w społeczeństwie jest równość a powinno być rozwarstwienie.";
            element_6[12] = "co prowadziu Polskę do katastrofy.";
            element_6[13] = "- dlatego trzeba przywrócić normalność.";
            element_6[14] = "ale w wolnej Polsce pójdą siedzieć.";
            element_6[15] = "przez kolejne kadencje.";
            element_6[16] = "o czym się nie mówi.";
            element_6[17] = "i właśnie dlatego Europa umiera.";
            element_6[18] = "ale przyjdą muzułumanie i zrobią prządek.";
            element_6[19] = "- tak samo zresztą jak za Hitlera.";
            element_6[20] = "- proszę zobaczyć, co się dzieje na Zachodzie, jeśli mi państwo nie wierzą.";
            element_6[21] = "co lat temu sto nikomu nie przyszło by do głowy.";

        }


        public String speechGenerator()
        {
            Random rnd = new Random();
            int index_1 = rnd.Next(0, 21);
            int index_2 = rnd.Next(0, 21);
            int index_3 = rnd.Next(0, 21);
            int index_4 = rnd.Next(0, 21);
            int index_5 = rnd.Next(0, 21);
            int index_6 = rnd.Next(0, 21);
            hit = (index_1 + index_2 + index_3 + index_4 + index_5 + index_6 + 1)*2;
            return element_1[index_1] + element_2[index_2] + element_3[index_3] + element_4[index_4] + element_5[index_5] + element_6[index_6];
        }

        public double getHit()
        {
            double tmp = hit;
            hit = 0;
            return tmp;
        }
            
    }
}
