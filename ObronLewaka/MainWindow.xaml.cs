﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ObronLewaka
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public static class ExtensionMethods
    {

        private static Action EmptyDelegate = delegate () { };


        public static void Refresh(this UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }
    }
    public partial class MainWindow : Window
    {
   
        private Korwin king = new Korwin();
        private static System.Timers.Timer aTimer;
        private MediaPlayer mediaPlayerYellow = new MediaPlayer();
        private MediaPlayer mediaPlayerSmiech = new MediaPlayer();
        private MediaPlayer mediaPlayerKurwa = new MediaPlayer();
        private MediaPlayer mediaPlayerUSRR = new MediaPlayer();


        Dictionary <String, int> wordList =  new Dictionary<String, int>();

        private String old_opinion = "";


        public MainWindow()
        {
           
            InitializeComponent();
            //  Korwin king = new Korwin();
            //  Debug.WriteLine(king.speechGenerator());
            KorwinIMG.Source = GetImage("Images/korwin_default.png");
            flaga.Source = GetImage("Images/rainbowfilter.png");
            opinia.IsEnabled = false;
            updateWords();
            Uri uri1 = new Uri("pack://siteoforigin:,,,/" + "Music/yellow.mp3", UriKind.RelativeOrAbsolute); // Implicit file path.
            Uri uri2 = new Uri("pack://siteoforigin:,,,/" + "Music/smiech.mp3", UriKind.RelativeOrAbsolute); // Implicit file path.
            Uri uri3 = new Uri("pack://siteoforigin:,,,/" + "Music/kurwa.mp3", UriKind.RelativeOrAbsolute); // Implicit file path.
            Uri uri4 = new Uri("pack://siteoforigin:,,,/" + "Music/ussr.mp3", UriKind.RelativeOrAbsolute); // Implicit file path.
            mediaPlayerYellow.Open(uri1);
            mediaPlayerSmiech.Open(uri2);
            mediaPlayerKurwa.Open(uri3);
            mediaPlayerUSRR.Open(uri4);
        }


        private static void SetTimer(Func<int, int> myMethodName)
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(250);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e, myMethodName);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
      
        }

        public int setTimeBar(int i)
        {
            this.Dispatcher.Invoke(() =>
            {
                TimeBar.Value -= i;
                if(TimeBar.Value==0)
                {
                    TimeBar.Value = 60;
                    WypowiedzKorwina.Text = king.speechGenerator();
                    double health = LewakHealth.Value;
                    double hit = king.getHit();

                    if (hit > 15)
                    {
                        mediaPlayerYellow.Pause();

                       
                        for (int k = 0; k < 4; k++)
                        {
                            mediaPlayerSmiech.Play();
                            mediaPlayerSmiech.Position = TimeSpan.Zero;
                        }
                   

                        for (int k = 0; k < 5000; k++)
                        {

                         
                                KorwinIMG.Source = GetImage("Images/korwin_zadowolony.png");
                                KorwinIMG.Refresh();
                        }
                        mediaPlayerYellow.Play();
                        KorwinIMG.Source = GetImage("Images/korwin_default.png");
                    }

                    health = health - (hit / 10);
                    if (health < 0)
                    {

                        health = 0;
                    }

                    LewakHealth.Value = health;

                    if (health <= 100)
                    {
                        //Foreground="Orange" 
                        LewakHealth.Foreground = Brushes.Orange;
                    }

                    if (health < 20)
                    {
                        LewakHealth.Foreground = Brushes.Red;
                    }

                    if (health == 0)
                    {
                        lewakZ.Visibility = Visibility.Visible;
                        opinia.IsEnabled = false;
                        aTimer.Stop();
                        aTimer.Dispose();
                        mediaPlayerYellow.Stop();
                        StartBtn.Content = "Zagraj jeszcze raz";
                        StartBtn.Visibility = Visibility.Visible;

                    }
                }
            });

            return 0;
        }



        private static void OnTimedEvent(Object source, ElapsedEventArgs e, Func<int, int> setTimeBar)
        {
                setTimeBar(1);
        }

        void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                aTimer.Stop();
                aTimer.Dispose();
            } catch(Exception ee)
            {

            }
            
        }


        void GenerateText(object sender, RoutedEventArgs e)
        {
            Go();
        }

        void StartGame(object sender, RoutedEventArgs e)
        {
            opinia.IsEnabled = true;
            WypowiedzKorwina.Text = king.speechGenerator();
            KorwinIMG.Source = GetImage("Images/korwin_default.png");
            SetTimer(setTimeBar);
            StartBtn.Visibility = Visibility.Hidden;
            korwinZ.Visibility = Visibility.Hidden;
            lewakZ.Visibility = Visibility.Hidden;
            KorwinHealth.Value = 500;
            LewakHealth.Value = 200;
            LewakHealth.Foreground = Brushes.Green;
            KorwinHealth.Foreground = Brushes.Green;
            mediaPlayerYellow.Position = TimeSpan.Zero;
            mediaPlayerKurwa.Position = TimeSpan.Zero;
            mediaPlayerSmiech.Position = TimeSpan.Zero;
            mediaPlayerUSRR.Position = TimeSpan.Zero;
            mediaPlayerYellow.Play();

        }


        void Go()
        {
            WypowiedzKorwina.Text = king.speechGenerator();
            TimeBar.Value = 60;
            double health = KorwinHealth.Value;
            double hit = provideFeedback();
            if(hit>25)
            {
                mediaPlayerYellow.Pause();
                mediaPlayerKurwa.Play();
                mediaPlayerKurwa.Position = TimeSpan.Zero;
                for (int i=0; i<5000;i++)
                {
                    KorwinIMG.Source = GetImage("Images/kowin_zdziwiony.png");
                    KorwinIMG.Refresh();
                }
                mediaPlayerYellow.Play();
                KorwinIMG.Source = GetImage("Images/korwin_default.png");
            }
            health -= hit;
            if (health < 0)
            {

                health = 0;
            }
            KorwinHealth.Value = health;


            if(health<=250)
            {
                //Foreground="Orange" 
                KorwinHealth.Foreground = Brushes.Orange;
            }

            if(health<90)
            {
                KorwinHealth.Foreground = Brushes.Red;
            }

            if (health==0)
            {
                korwinZ.Visibility = Visibility.Visible;
                KorwinIMG.Source = GetImage("Images/korwin_flaga.png");
                mediaPlayerUSRR.Play();
                aTimer.Stop();
                aTimer.Dispose();
                mediaPlayerYellow.Stop();
                StartBtn.Content = "Zagraj jeszcze raz";
                StartBtn.Visibility = Visibility.Visible;
                return;
            }

 

     
        }


        public void updateWords()
        {
            wordList.Add("Pan", 20);
            wordList.Add("tarszych", 400);
            wordList.Add("ludz", 15);
            wordList.Add("przegrał", 7);
            wordList.Add("wybory", 15);
            wordList.Add("wyborach", 18);
            wordList.Add("tegorocznych", 18);
            wordList.Add("przegrać", 20);
            wordList.Add("rzędu", 2);
            wordList.Add("raz", 1);
            wordList.Add("lgbt", 50);
            wordList.Add("LGBT", 80); //to
            wordList.Add("kooalicja", 15);
            wordList.Add("gej", 30);
            wordList.Add("lesbijk", 48);
            wordList.Add("polityce", 14);
            wordList.Add("racje", 21);
            wordList.Add("nie", 30);
            wordList.Add("?", 40);
            wordList.Add("myśli", 30);
            wordList.Add("wolność słowa", 30);
            wordList.Add("mam", 3);
            wordList.Add("znowu", 4);
            wordList.Add("spierdalaj", 100);
            wordList.Add("spiepszaj", 80);
            wordList.Add("dziadu", 159);
            wordList.Add("niemiły", 7);
            wordList.Add("obrażliwe", 18);
            wordList.Add("ignorantem", 7);
            wordList.Add("pokemonem", 30);
            wordList.Add("jest", 4);
            wordList.Add("Jak", 14);
            wordList.Add("jak", 14);
            wordList.Add("wstyd", 28);
            //wordList.Add("nie", 17);
            wordList.Add("poprawna", 29);
            wordList.Add("debili", 5);
            wordList.Add("odmiennej", 50);
            wordList.Add("homoseksualne", 40);
            wordList.Add("olskę", 15);
            wordList.Add("tyranie", 150);
            wordList.Add("komunizm", 8);
            wordList.Add("upokorzył", 50);
            wordList.Add("uropejsk", 150); //to
            wordList.Add("itller", 300); //to
            wordList.Add("500+", 40);
            wordList.Add("dla", 1);
            wordList.Add("inne", 5);
            wordList.Add("zasiłki", 70); //to
            wordList.Add("pomogły", 50);
            wordList.Add("psychiatr", 70);
            wordList.Add("komunista", 6); //To
            wordList.Add("wyzywać", 17);
            wordList.Add("huj", 30);
            wordList.Add("jebany", 8);
            wordList.Add("dupę", 18);
            wordList.Add("!", 4);
            wordList.Add(".", 1);
            wordList.Add("demkokracja", 56);
            wordList.Add("krypto", 36);
            wordList.Add("co", 4);
            wordList.Add("Co", 6);
            wordList.Add("głupoty", 78);
            wordList.Add("opowiada", 10);
            wordList.Add("katolicy", 15);
            wordList.Add("aczyński", 18);
            wordList.Add("iedroń", 100);
            wordList.Add("rację", 8);
            wordList.Add("fakt", 15);
          
            wordList.Add("Jest", 8);
            wordList.Add("ruskim", 25);
            wordList.Add("rosyjskim", 30);
            wordList.Add("agentem", 40);
            wordList.Add("szpiegiem", 45);
            wordList.Add("Żyd", 15);
            wordList.Add("żyd", 14);
            wordList.Add("wiosn", 30);
            wordList.Add("andberg", 50);
            wordList.Add("wolny rynek", 100);
            wordList.Add("pomyłka", 10);
        }


        void EnterClicked(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Go();
                e.Handled = true;
            }
        }

        int provideFeedback()
        { 

            if(opinia.Text.Length == 0)
            {
                return 0;
            }

            String feed_back = opinia.Text;
            if(feed_back == old_opinion)
            {
                return 0;
            }

            old_opinion = feed_back;

            int hit = 0;
            foreach(var word in wordList)
            {
                if(feed_back.IndexOf(word.Key)>-1)
                {
                    hit += word.Value;
                }
            }

            opinia.Text = "";
            if(hit>450)
            {
                //Bonus
                hit += 200;
            }
            return hit;
        }


        private static BitmapImage GetImage(string imageUri)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri("pack://siteoforigin:,,,/" + imageUri, UriKind.RelativeOrAbsolute);
            bitmapImage.EndInit();
            return bitmapImage;
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}
